package com.example.modelmapper.dtos;

import java.util.ArrayList;
import java.util.List;

public class StudentRequest {
    public StudentRequest() {

    }

    private Long id ;
    private String firstname;
    private String lastname;

    private List<AddressRequest> addresses ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<AddressRequest> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressRequest> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "StudentRequest{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", addresses=" + addresses +
                '}';
    }
}
