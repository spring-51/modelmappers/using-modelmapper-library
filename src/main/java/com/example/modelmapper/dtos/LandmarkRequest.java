package com.example.modelmapper.dtos;

public class LandmarkRequest {
    private Long id;
    private String name ;

    public LandmarkRequest() {
    }

    public LandmarkRequest(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LandmarkRequest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
