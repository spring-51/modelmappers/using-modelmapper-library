package com.example.modelmapper.dtos;

import java.util.ArrayList;
import java.util.List;

public class AddressRequest {
    private Long id;
    private String streetName;
    private List<LandmarkRequest> landmarks = new ArrayList<>();

    public AddressRequest() {
    }

    public AddressRequest(Long id, String streetName) {
        this.id = id;
        this.streetName = streetName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public List<LandmarkRequest> getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(List<LandmarkRequest> landmarks) {
        this.landmarks = landmarks;
    }

    @Override
    public String toString() {
        return "AddressRequest{" +
                "id=" + id +
                ", streetName='" + streetName + '\'' +
                ", landamarks=" + landmarks +
                '}';
    }
}
