package com.example.modelmapper.dtos;

import java.util.List;

public class AddressResponse{
    private Long id;
    private String streetName;
    private List<LandmarkResponse> landmarks;

    public List<LandmarkResponse> getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(List<LandmarkResponse> landmarks) {
        this.landmarks = landmarks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    @Override
    public String toString() {
        return "AddressResponse{" +
                "id=" + id +
                ", streetName='" + streetName + '\'' +
                ", landmarks=" + landmarks +
                '}';
    }
}
