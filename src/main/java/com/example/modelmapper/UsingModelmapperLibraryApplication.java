package com.example.modelmapper;

import com.example.modelmapper.dtos.*;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class UsingModelmapperLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsingModelmapperLibraryApplication.class, args);

        // mapper demo
        /*

        StudentRequest request = new StudentRequest();
        request.setFirstname("firstName");
        request.setLastname("lastName");
        StudentResponse map = new ModelMapper().map(request, StudentResponse.class);
        System.out.println(map);

         */
    }

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

}
