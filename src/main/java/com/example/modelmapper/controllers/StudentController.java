package com.example.modelmapper.controllers;

import com.example.modelmapper.dtos.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private ModelMapper mapper;

    @PostMapping
    public StudentResponse post( @RequestBody StudentRequest request){
        StudentResponse response = mapper.map(request, StudentResponse.class);
        return response;
    }
}
