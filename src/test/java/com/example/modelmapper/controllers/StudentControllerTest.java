package com.example.modelmapper.controllers;

import com.example.modelmapper.dtos.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class StudentControllerTest {

    @Autowired
    private StudentController controller;

    @Test
    public void postTest(){
        StudentRequest request = new StudentRequest();
        request.setId(1l);
        request.setFirstname("testFirstname");
        request.setLastname("testLastname");

        AddressRequest addRequest1 = new AddressRequest();
        addRequest1.setId(2l);
        addRequest1.setStreetName("wakad");

        LandmarkRequest landmarkRequest1 = new LandmarkRequest();
        landmarkRequest1.setName("newlandmark");
        landmarkRequest1.setId(3l);
        addRequest1.getLandmarks().add(landmarkRequest1);

        LandmarkRequest landmarkRequest2 = new LandmarkRequest();
        landmarkRequest2.setName("newlandmark2");
        landmarkRequest2.setId(33l);
        addRequest1.getLandmarks().add(landmarkRequest2);

        request.setAddresses(Arrays.asList(addRequest1));
        StudentResponse response = controller.post(request);

        assertEquals(request.getId(),response.getId());
        assertEquals(request.getFirstname(),response.getFirstname());
        assertEquals(request.getLastname(),response.getLastname());
        List<AddressResponse> respAddresses = response.getAddresses();
        List<AddressRequest> reqAddresses = request.getAddresses();
        assertEquals(reqAddresses.size(), respAddresses.size());

        for(int i = 0 ; i< respAddresses.size(); i++){
            assertEquals(reqAddresses.get(i).getId(), respAddresses.get(i).getId());
            assertEquals(reqAddresses.get(i).getStreetName(), respAddresses.get(i).getStreetName());
            assertEquals(reqAddresses.get(i).getLandmarks().size(), respAddresses.get(i).getLandmarks().size());
            List<LandmarkRequest> reqAddrlandmarks = reqAddresses.get(i).getLandmarks();
            List<LandmarkResponse> respAddrlandmarks = respAddresses.get(i).getLandmarks();

            for(int j =0; j< respAddrlandmarks.size();j++){
                assertEquals(reqAddrlandmarks.get(i).getId(), respAddrlandmarks.get(i).getId());
                assertEquals(reqAddrlandmarks.get(i).getName(), respAddrlandmarks.get(i).getName());
            }
        }
    }
}
