# Java Model mapping using ModelMapper library

## Refrences
```ref
refer - http://modelmapper.org/getting-started/
```

## maven dependency
```md
1. 
<dependency>
    <groupId>org.modelmapper</groupId>
    <artifactId>modelmapper</artifactId>
    <version>2.3.5</version>
</dependency>

- refer - ./pom.xml
```


## API end point
```
POST {baseurl}/students
eg. POST 127.0.0.1:8080/students
```

## Request Body
```request
{
    "id":1,
    "firstname":"kaush",
    "lastname":"singh" ,
    "addresses" : [{
        "id": 2,
        "streetname":"wakad",
        "landmarks":[{
            "id":3,
            "name":"new landmark"
        }]
    },{
        "id": 22,
        "streetname":"wakadd",
        "landmarks":[{
            "id":33,
            "name":"new landmarkk"
        }]
    }]
}
```

## Response Body
As per the Request we will get the output, for above request we will get
```res
{
    "id": 1,
    "firstname": "kaush",
    "lastname": "singh",
    "addresses": [
        {
            "id": 2,
            "streetName": null,
            "landmarks": [
                {
                    "id": 3,
                    "name": "new landmark"
                }
            ]
        },
        {
            "id": 22,
            "streetName": null,
            "landmarks": [
                {
                    "id": 33,
                    "name": "new landmarkk"
                }
            ]
        }
    ]
}
```

## Junit tests

```
1. StudentControllerTest
```

## Pending
```
1. Array mapping,
2. Map<K,V> ,mapping
3. if variable names are different in source and target type
```
